var express     = require('express');
var bodyParser  = require('body-parser');
var http        = require('http');
var cluster     = require('cluster');
var numCPUs     = require('os').cpus().length;

if (cluster.isMaster) {
  for (var i = 0; i < numCPUs; i++) {
    cluster.fork();
  }
} else {
  function randomStr (size) {
    var str = '';
    var a = 'A'.charCodeAt(0);
    var z = 'Z'.charCodeAt(0);
    while (str.length < size) {
      str += String.fromCharCode(Math.random() * (z - a + 1) + a);
    }
    return str;
  }

  function randomObj (size) {
    var obj = {};
    while (JSON.stringify(obj).length < size) {
      var len = Math.random() * size / 10;
      obj[randomStr(10)] = ~~(Math.random() * 2) % 2 === 0 ? randomStr(len) : randomObj(len);
    }
    return obj;
  }
  var app = express();
  var obj = randomObj(3000);

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));

  app.post('/', function (req, res) {
    console.log('Connection handled');
    setTimeout(function () {return res.status(200).send(req.body)}, Math.random() * 2000 + 1000);
  });

  app.get('/', function (req, res) {
    return setTimeout(function () {res.status(200).json(randomObj(3000))}, Math.random() * 2000 + 1000);
  })

  var srv = http.createServer(app);

  srv.listen(3001);
}
