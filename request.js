var request = require('request');
var should = require('should');

function randomStr (size) {
  var str = '';
  var a = 'A'.charCodeAt(0);
  var z = 'Z'.charCodeAt(0);
  while (str.length < size) {
    str += String.fromCharCode(Math.random() * (z - a + 1) + a);
  }
  return str;
}

function randomObj (size) {
  var obj = {};
  while (JSON.stringify(obj).length < size) {
    var len = Math.random() * size / 10;
    obj[randomStr(10)] = ~~(Math.random() * 2) % 2 === 0 ? randomStr(len) : randomObj(len);
  }
  return obj;
}

var stats = {
  totalSize: 0,
  startTime: Date.now(),
  totalReqs: 0,
  failedReqs: 0
};

function send () {
  var obj = randomObj(3000);
  console.log('Sending %d bytes of JSON', JSON.stringify(obj).length);
  request({
    url: process.argv[2],
    method: 'POST',
    body: JSON.stringify(obj),
    headers: {
      'Content-Type': 'application/json'
    }
  }, function (err, res, body) {
    if (err || res.statusCode !== 200) {
      console.error(err);
      console.log(body);
      stats.failedReqs++;
    } else {
      res.statusCode.should.be.equal(200);
      var data = JSON.parse(body);
      data.should.be.deepEqual(obj);
      stats.totalSize += JSON.stringify(obj).length;
      stats.totalReqs++;
    }
    return send();
  });
}

(function wait () {
  if (true) setTimeout(wait, 1000);
})();

for (var i = 0; i < 100; i++) 
  send();

function logResult() {
  var time = Date.now() - stats.startTime;
  console.log('Totally sended %d bytes of json by %d requests', stats.totalSize, stats.totalReqs);
  console.log('Requests failed: %d (%d% of total)', stats.failedReqs, (stats.failedReqs / stats.totalReqs * 100).toFixed(2));
  console.log('Time spent: %d ms', time);
  console.log('Speed: %d Kb/s, %d req/s', (stats.totalSize / time).toFixed(2), (stats.totalReqs / time * 1000).toFixed(2));
  process.exit();
}

process.on('SIGINT', logResult);
